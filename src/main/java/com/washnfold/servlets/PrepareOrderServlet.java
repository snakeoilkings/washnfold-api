package com.washnfold.servlets;

import com.washnfold.dao.DetergentDao;
import com.washnfold.dao.DropOffMethodDao;
import com.washnfold.dao.FabricSoftenerDao;
import com.washnfold.dao.PickUpMethodDao;
import com.washnfold.entities.Detergent;
import com.washnfold.entities.DropOffMethod;
import com.washnfold.entities.FabricSoftener;
import com.washnfold.entities.PickUpMethod;

import javax.servlet.annotation.WebServlet;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/startOrder")
public class PrepareOrderServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        DetergentDao detergentDao = new DetergentDao();
        DropOffMethodDao dropOffDao = new DropOffMethodDao();
        FabricSoftenerDao fabricSoftenerDao = new FabricSoftenerDao();
        PickUpMethodDao pickUpMethodDao = new PickUpMethodDao();

        try {
            ArrayList<Detergent> detergentList = detergentDao.list();
            request.setAttribute("detergentList", detergentList);

            ArrayList<DropOffMethod> dropOffMethodList = dropOffDao.list();
            request.setAttribute("dropOffMethodList", dropOffMethodList);

            ArrayList<FabricSoftener> fabricSoftenerList = fabricSoftenerDao.list();
            request.setAttribute("fabricSoftenerList", fabricSoftenerList);

            ArrayList<PickUpMethod> pickUpMethodList = pickUpMethodDao.list();
            request.setAttribute("pickUpMethodList", pickUpMethodList);

            RequestDispatcher dispatcher = request.getRequestDispatcher("order.jsp");
            dispatcher.forward(request, response);

        } catch (SQLException e) {
            e.printStackTrace();
            throw new ServletException(e);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
