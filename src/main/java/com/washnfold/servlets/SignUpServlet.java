package com.washnfold.servlets;

import com.washnfold.dao.CustomerDao;
import com.washnfold.entities.Customer;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;

public class SignUpServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        boolean success = false;
        String remoteHost = request.getRemoteHost(); //TODO: redirect to jsp if endpoint is ONLY hit by web application, not mobile

        String email = request.getParameter("email");
        String password = request.getParameter("password");
        String firstName = request.getParameter("firstName");
        String lastName = request.getParameter("lastName");
        String phoneNumber = request.getParameter("phoneNumber");
        String street = request.getParameter("street");
        String apartment = request.getParameter("apartment");
        String city = request.getParameter("city");
        String state = request.getParameter("state");
        String zipcode = request.getParameter("zipcode");

        HttpSession session =request.getSession();
        String forwardTo = "/adduser-failure.jsp";
        long accountID = -1;

        Customer newCustomer = new Customer(email, password, phoneNumber, firstName, lastName, street, apartment, city,
                zipcode, state);

        // create the new user
        try {
            success = CustomerDao.createCustomer(newCustomer);

            // get the accountID
            if (success) {
                newCustomer = CustomerDao.fetchCustomer(email,password);
                session.setAttribute("accountID", newCustomer.getAccountID());

                forwardTo = "/adduser-success.jsp";
            }
        } catch (SQLException e) {
            forwardTo = "/adduser-failure.jsp";
             }
        catch (ClassNotFoundException e) {
            forwardTo = "/adduser-failure.jsp";
        }

        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(forwardTo);
        dispatcher.forward(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
