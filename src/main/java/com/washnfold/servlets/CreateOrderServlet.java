package com.washnfold.servlets;

import com.washnfold.dao.CustomerDao;
import com.washnfold.dao.OrderDao;
import com.washnfold.entities.Customer;
import com.washnfold.entities.Order;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@WebServlet("/submitOrder")
public class CreateOrderServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String remoteHost = request.getRemoteHost(); //TODO: redirect to jsp if endpoint is ONLY hit by web application, not mobile

        HttpSession session = request.getSession();
        String forwardTo = "/addorder-failure.jsp";
        boolean success = false;

        Long accountID = (Long)session.getAttribute("accountID");
        String deliveryAddress = (String)session.getAttribute("deliveryAddress");
        String deliveryApartment = (String)session.getAttribute("deliveryApartment");
        String deliveryCounty = (String)session.getAttribute("deliveryCounty");
        String deliveryState = (String)session.getAttribute("deliveryState");
        String deliveryZipcode = (String)session.getAttribute("deliveryZipcode");

        Order order = null;

        int detergentType = Integer.parseInt(request.getParameter("detergent"));
        int fabricSoftenerType = Integer.parseInt(request.getParameter("fabricSoftener"));
        int pickUpMethodType = Integer.parseInt(request.getParameter("pickUpMethod"));
        int dropOffMethodType = Integer.parseInt(request.getParameter("dropOffMethod"));

        boolean separateWhites = Boolean.parseBoolean(request.getParameter("separateWhites"));
        boolean separateLoads = Boolean.parseBoolean(request.getParameter("separateBags"));

        String pickUpStr = request.getParameter("pickupTime");
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm");
        LocalDateTime pickUpTime = LocalDateTime.parse(pickUpStr, formatter);

        String specialInstructions = request.getParameter("specialInstructions");

        order = new Order(accountID, detergentType, fabricSoftenerType, pickUpMethodType, dropOffMethodType,
                separateWhites, separateLoads, deliveryAddress, deliveryApartment, deliveryCounty, deliveryState,
                deliveryZipcode, specialInstructions, pickUpTime);

        // create the order
        try {
            success = OrderDao.createOrder(order);

            if (success) {
                forwardTo = "/addorder-success.jsp";
            }
        } catch (SQLException e) {
            // TODO
        }
        catch (ClassNotFoundException e) {
            // TODO
        }

        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(forwardTo);
        dispatcher.forward(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
