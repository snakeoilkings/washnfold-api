package com.washnfold.servlets;

import com.washnfold.dao.CustomerDao;
import com.washnfold.entities.Customer;

import javax.jws.WebService;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String email = request.getParameter("email");
        String password = request.getParameter("password");
        HttpSession session = request.getSession();

        Customer customer = null;
        try {
            customer = CustomerDao.fetchCustomer(email,password);
        } catch (SQLException e) {
            // TODO
        } catch (ClassNotFoundException e) {
            // TODO
        }
        session.setAttribute("accountID", customer.getAccountID());
        session.setAttribute("deliveryAddress", customer.getStreet());
        session.setAttribute("deliveryApartment", customer.getApartment());
        session.setAttribute("deliveryCity", customer.getCity());
        session.setAttribute("deliveryState", customer.getState());
        session.setAttribute("deliveryZipcode", customer.getZipcode());

        String forwardTo = "/index.jsp";
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(forwardTo);
        dispatcher.forward(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
