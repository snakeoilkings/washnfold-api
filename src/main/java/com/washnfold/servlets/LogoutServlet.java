package com.washnfold.servlets;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class LogoutServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        // remove accountID
        HttpSession session =request.getSession();
        session.setAttribute("accountID", null);

        String forwardTo = "/index.jsp";
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(forwardTo);
        dispatcher.forward(request,response);
    }
}
