package com.washnfold.entities;

public class DefaultSettings {

    private long accountID;

    private int detergentType;
    private int fabricSoftenerType;
    private boolean separateWhites;
    private boolean separateLoads;

    private String deliveryAddress;
    private String deliveryCounty;
    private String zipCode;
    private String state;
    private String specialInstructions;

    public DefaultSettings(long accountID) {
        this.accountID = accountID;
    }


    public void setDetergentType(int detergentType) {
        this.detergentType = detergentType;
    }

    public int getDetergentType() {
        return detergentType;
    }

    public void setFabricSoftenerType(int fabricSoftenerType) {
        this.fabricSoftenerType = fabricSoftenerType;
    }

    public int getFabricSoftenerType() {
        return fabricSoftenerType;
    }

    public void setSeparateWhites(boolean separateWhites) {
        this.separateWhites = separateWhites;
    }

    public boolean getSeparateWhites() {
        return separateWhites;
    }

    public void setSeparateLoads(boolean separateLoads) {
        this.separateLoads = separateLoads;
    }

    public void setDeliveryAddress(String deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    public String getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryCounty(String county) {
        this.deliveryCounty = county;
    }

    public String getDeliveryCounty() {
        return deliveryCounty;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getState() {
        return state;
    }

    public void setSpecialInstructions(String specialInstructions) {
        this.specialInstructions = specialInstructions;
    }

    public String getSpecialInstructions() {
        return specialInstructions;
    }
}
