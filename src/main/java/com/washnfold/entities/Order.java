package com.washnfold.entities;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;

public class Order {

    private long orderID;
    private long accountID;

    private double price;

    private int detergentType;
    private int fabricSoftenerType;
    private int pickUpMethodType;
    private int dropOffMethodType;

    private boolean separateWhites;
    private boolean separateLoads;

    private LocalDateTime orderTime;
    private LocalDateTime pickUpTime;
    private LocalDateTime dropOffTime;

    private String deliveryAddress;
    private String deliveryApartment;
    private String deliveryCity;
    private String deliveryZipCode;
    private String deliveryState;
    private String specialInstructions;

    public Order(long accountID, int detergentType, int fabricSoftenerType, int pickUpMethodType, int dropOffMethodType,
                 boolean separateWhites, boolean separateLoads, String deliveryAddress, String deliveryApartment,
                 String deliveryCity, String deliveryState, String deliveryZipCode, String specialInstructions,
                 LocalDateTime pickUpTime) {
        this.accountID = accountID;
        this.detergentType = detergentType;
        this.fabricSoftenerType = fabricSoftenerType;
        this.pickUpMethodType = pickUpMethodType;
        this.dropOffMethodType = dropOffMethodType;
        this.separateWhites = separateWhites;
        this.separateLoads = separateLoads;
        this.deliveryAddress = deliveryAddress;
        this.deliveryApartment = deliveryApartment;
        this.deliveryCity = deliveryCity;
        this.deliveryState = deliveryState;
        this.deliveryZipCode = deliveryZipCode;
        this.specialInstructions = specialInstructions;
        this.orderTime = LocalDateTime.now();
        this.pickUpTime = pickUpTime;
    }

    public void setAccountID(long accountID) {
        this.accountID = accountID;
    }

    public long getAccountID() {
        return accountID;
    }

    public long getOrderID() {
        return orderID;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getPrice() {
        return price;
    }

    public void setDetergentType(int detergentType) {
        this.detergentType = detergentType;
    }

    public int getDetergentType() {
        return detergentType;
    }

    public void setFabricSoftenerType(int fabricSoftenerType) {
        this.fabricSoftenerType = fabricSoftenerType;
    }

    public int getFabricSoftenerType() {
        return fabricSoftenerType;
    }

    public void setPickUpMethodType(int pickUpMethodType) {
        this.pickUpMethodType = pickUpMethodType;
    }

    public int getPickUpMethodType() {
        return pickUpMethodType;
    }

    public void setDropOffMethodType(int dropOffMethodType) {
        this.dropOffMethodType = dropOffMethodType;
    }

    public int getDropOffMethodType() {
        return dropOffMethodType;
    }

    public void setSeparateWhites(boolean separateWhites) {
        this.separateWhites = separateWhites;
    }

    public boolean getSeparateWhites() {
        return separateWhites;
    }

    public void setSeparateLoads(boolean separateLoads) {
        this.separateLoads = separateLoads;
    }

    public boolean getSeparateLoads() {
        return separateLoads;
    }

    public void setDeliveryAddress(String deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    public String getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryApartment(String deliveryApartment) {
        this.deliveryApartment = deliveryApartment;
    }

    public String getDeliveryApartment() {
        return deliveryApartment;
    }

    public void setDeliveryCity(String deliveryCity) {
        this.deliveryCity = deliveryCity;
    }

    public String getDeliveryCity() {
        return deliveryCity;
    }

    public void setDeliveryZipCode(String deliveryZipcode) {
        this.deliveryZipCode = deliveryZipcode;
    }

    public String getDeliveryZipCode() {
        return deliveryZipCode;
    }

    public void setDeliveryState(String deliveryState) {
        this.deliveryState = deliveryState;
    }

    public String getDeliveryState() {
        return deliveryState;
    }

    public void setSpecialInstructions(String specialInstructions) {
        this.specialInstructions = specialInstructions;
    }

    public String getSpecialInstructions() {
        return specialInstructions;
    }

    public void setPickUpTime(LocalDateTime pickUpTime) {
        this.pickUpTime = pickUpTime;
    }

    public LocalDateTime getPickUpTime() { return pickUpTime; }

    public LocalDateTime getOrderTime() { return orderTime; }
}
