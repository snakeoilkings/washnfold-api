package com.washnfold.dao;

import com.washnfold.entities.Customer;
import com.washnfold.sql.ConnectionFactory;

import java.sql.*;

public class CustomerDao {

    public static Customer fetchCustomer(String email, String password) throws SQLException, ClassNotFoundException {
        Connection connection = ConnectionFactory.getConnection();

        try {
            String query = "SELECT * FROM customer WHERE email= ? AND password= ?";
            PreparedStatement stmt = connection.prepareStatement(query);
            stmt.setString(1, email);
            stmt.setString(2, password);

            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                Customer user = new Customer();
                user.setAccountID(rs.getLong("accountID"));
                user.setEmail(rs.getString("email"));
                user.setFirstName(rs.getString("firstName"));
                user.setLastName(rs.getString("lastName"));
                user.setStreet(rs.getString("street"));
                user.setApartment(rs.getString("apartment"));
                user.setCity(rs.getString("city"));
                user.setZipcode(rs.getString("zipcode"));
                user.setState(rs.getString("state"));
                user.setPhoneNumber(rs.getString("phoneNumber"));
                connection.close();

                return user;
            }
        } catch(Exception e) {
            return null;
        } finally {
            connection.close();
        }
        return null;
    }


    public static Customer fetchCustomerAddress(int accountID) throws SQLException, ClassNotFoundException {
        Connection connection = ConnectionFactory.getConnection();

        try {
            String query = "SELECT street, apartment, city, state, zipcode FROM customer WHERE accountID = ?";
            PreparedStatement stmt = connection.prepareStatement(query);
            stmt.setString(1, Integer.toString(accountID));

            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                Customer user = new Customer();
                user.setStreet(rs.getString("street"));
                user.setApartment(rs.getString("apartment"));
                user.setCity(rs.getString("city"));
                user.setZipcode(rs.getString("zipcode"));
                user.setState(rs.getString("state"));
                connection.close();

                return user;
            }
        } catch(Exception e) {
            return null;
        } finally {
            connection.close();
        }
        return null;
    }


    public static boolean createCustomer(Customer user) throws SQLException, ClassNotFoundException {
        Connection connection = ConnectionFactory.getConnection();

        try {
            String query = "INSERT INTO customer (email, password, firstName, lastName, street, apartment, city, zipcode," +
                    " state, phoneNumber) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

            PreparedStatement stmt = connection.prepareStatement(query);
            stmt.setString(1, user.getEmail());
            stmt.setString(2, user.getPassword());
            stmt.setString(3, user.getFirstName());
            stmt.setString(4, user.getLastName());
            stmt.setString(5, user.getStreet());
            stmt.setString(6, user.getApartment());
            stmt.setString(7, user.getCity());
            stmt.setString(8, user.getZipcode());
            stmt.setString(9, user.getState());
            stmt.setString(10, user.getPhoneNumber());

            stmt.executeUpdate();
            return true;
        } catch(Exception e) {
            return false;
        } finally {
            connection.close();
        }
    }
}
