package com.washnfold.dao;

import com.washnfold.entities.FabricSoftener;
import com.washnfold.sql.ConnectionFactory;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class FabricSoftenerDao {

    public ArrayList<FabricSoftener> list() throws SQLException, ClassNotFoundException {
        Connection connection = ConnectionFactory.getConnection();

        ArrayList<FabricSoftener> fabricSoftenerList = new ArrayList<FabricSoftener>();

        try {
            String sql = "SELECT * FROM DimFabricSoftener ORDER BY fabricSoftenerName";
            Statement statement = connection.createStatement();
            ResultSet result = statement.executeQuery(sql);

            while (result.next()) {
                int id = result.getInt("FabricSoftenerID");
                String name = result.getString("FabricSoftenerName");
                FabricSoftener fabricSoftener = new FabricSoftener(id, name);

                fabricSoftenerList.add(fabricSoftener);
            }
        } catch(Exception e) {
            e.printStackTrace();
        } finally {
            connection.close();
        }

        return fabricSoftenerList;
    }
}
