package com.washnfold.dao;

import com.washnfold.entities.PaymentMethod;
import com.washnfold.sql.ConnectionFactory;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

// I don't think we will need this class but I'm making it right now anyway
public class PaymentMethodDao {

    public ArrayList<PaymentMethod> list() throws SQLException, ClassNotFoundException {
        Connection connection = ConnectionFactory.getConnection();

        ArrayList<PaymentMethod> paymentMethodList = new ArrayList<PaymentMethod>();

        try {
            String sql = "SELECT * FROM DimPaymentMethod ORDER BY name";
            Statement statement = connection.createStatement();
            ResultSet result = statement.executeQuery(sql);

            while (result.next()) {
                int id = result.getInt("PaymentMethodID");
                String name = result.getString("PaymentMethodName");
                PaymentMethod paymentMethod = new PaymentMethod(id, name);

                paymentMethodList.add(paymentMethod);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            connection.close();
        }

        return paymentMethodList;
    }
}