package com.washnfold.dao;

import com.washnfold.entities.Detergent;
import com.washnfold.sql.ConnectionFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DetergentDao {

    public ArrayList<Detergent> list() throws SQLException, ClassNotFoundException {
        Connection connection = ConnectionFactory.getConnection();

        ArrayList<Detergent> detergentList = new ArrayList<Detergent>();

        try {
            String sql = "SELECT * FROM DimDetergent ORDER BY detergentName";
            Statement statement = connection.createStatement();
            ResultSet result = statement.executeQuery(sql);

            while (result.next()) {
                int id = result.getInt("detergentID");
                String name = result.getString("detergentName");
                Detergent detergent = new Detergent(id, name);

                detergentList.add(detergent);
            }
        } catch(Exception e) {
            e.printStackTrace();
        } finally {
            connection.close();
        }

        return detergentList;
    }
}