package com.washnfold.dao;

import com.washnfold.entities.PickUpMethod;
import com.washnfold.sql.ConnectionFactory;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class PickUpMethodDao {

    public ArrayList<PickUpMethod> list() throws SQLException, ClassNotFoundException {
        Connection connection = ConnectionFactory.getConnection();

        ArrayList<PickUpMethod> pickUpMethodList = new ArrayList<PickUpMethod>();

        try {
            String sql = "SELECT * FROM DimPickUpMethod ORDER BY pickUpMethodName";
            Statement statement = connection.createStatement();
            ResultSet result = statement.executeQuery(sql);

            while (result.next()) {
                int id = result.getInt("PickUpMethodID");
                String name = result.getString("PickUpMethodName");
                PickUpMethod pickUpMethod = new PickUpMethod(id, name);

                pickUpMethodList.add(pickUpMethod);
            }
        } catch(Exception e) {
            e.printStackTrace();
        } finally {
            connection.close();
        }

        return pickUpMethodList;
    }
}
