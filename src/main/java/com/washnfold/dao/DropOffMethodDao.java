package com.washnfold.dao;

import com.washnfold.entities.Detergent;
import com.washnfold.entities.DropOffMethod;
import com.washnfold.sql.ConnectionFactory;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class DropOffMethodDao {

    public ArrayList<DropOffMethod> list() throws SQLException, ClassNotFoundException {
        Connection connection = ConnectionFactory.getConnection();

        ArrayList<DropOffMethod> dropOffMethodList = new ArrayList<DropOffMethod>();

        try {
            String sql = "SELECT * FROM DimDropOffMethod ORDER BY dropOffMethodName";
            Statement statement = connection.createStatement();
            ResultSet result = statement.executeQuery(sql);

            while (result.next()) {
                int id = result.getInt("DropOffMethodID");
                String name = result.getString("DropOffMethodName");
                DropOffMethod dropOffMethod = new DropOffMethod(id, name);

                dropOffMethodList.add(dropOffMethod);
            }
        } catch(Exception e) {
            e.printStackTrace();
        } finally {
            connection.close();
        }

        return dropOffMethodList;
    }
}
