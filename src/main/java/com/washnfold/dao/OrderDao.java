package com.washnfold.dao;

import com.washnfold.entities.Order;
import com.washnfold.sql.ConnectionFactory;

import java.sql.*;
import java.time.LocalDateTime;
import java.time.ZoneId;

public class OrderDao {

    public static boolean createOrder(Order order) throws SQLException, ClassNotFoundException {
        Connection connection = ConnectionFactory.getConnection();

        try {
            String query = "INSERT INTO orders (accountID, detergent, fabricSoftener, pickUpMethod, dropOffMethod, " +
                    "separateWhites, separateBags, deliveryAddress, deliveryApartment, deliveryCity, " +
                    "deliveryState, deliveryZipCode, specialInstructions, pickUpTime, orderTime) VALUES " +
                    "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

            PreparedStatement stmt = connection.prepareStatement(query);

            stmt.setLong(   1, order.getAccountID());
            stmt.setInt(    2, order.getDetergentType());
            stmt.setInt(    3, order.getFabricSoftenerType());
            stmt.setInt(    4, order.getPickUpMethodType());
            stmt.setInt(    5, order.getDropOffMethodType());
            stmt.setInt(    6, order.getSeparateWhites() ? 1 : 0);
            stmt.setInt(    7, order.getSeparateLoads() ? 1 : 0);
            stmt.setString( 8, order.getDeliveryAddress());
            stmt.setString( 9, order.getDeliveryApartment());
            stmt.setString(10, order.getDeliveryCity());
            stmt.setString(11, order.getDeliveryState());
            stmt.setString(12, order.getDeliveryZipCode());
            stmt.setString(13, order.getSpecialInstructions());
            stmt.setTimestamp(14, Timestamp.valueOf(order.getPickUpTime()));
            stmt.setTimestamp(15, Timestamp.valueOf(order.getOrderTime()));

            stmt.executeUpdate();
            return true;
        } catch(Exception e) {
            return false;
        } finally {
            connection.close();
        }
    }
}
