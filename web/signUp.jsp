<%--
  Created by IntelliJ IDEA.
  User: conoz
  Date: 3/16/2019
  Time: 7:32 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>L.I. Wash & Fold</title>

    <script>
        function pass() {
            var pass_1 = document.forms["signup"].password.value.length;
            var pass_2 = document.forms["signup"].password_confirm.value.length;
            if (pass_1 != pass_2) {
                alert("the passwords do not match");
            } else {
                document.forms["signup"].submit();
            }
        }
    </script>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>
Sign up for a new account using your email address:

<form action="./signup" id="signup" method="post" onsubmit="pass()">
    <label>
        Email Address
        <input type="text" name="email" required/>
    </label>
    <br/>
    <label>
        Password
        <input type="password" name="password" required/>
    </label>
    <br/>
    <label>
        Confirm Password
        <input type="password" name="password_confirm" required/>
    </label>
    <br/>
    <label>
        Street Address
        <input type="text" name="street" required/>
    </label>
    <br/>
    <label>
        Apartment
        <input type="text" name="apartment" />
    </label>
    <br/>
    <label>
        City
        <input type="text" name="city" required/>
    </label>
    <br/>
    <label>
        State
        <input type="text" name="state" required/>
    </label>
    <br/>
    <label>
        Zip Code
        <input type="text" name="zipcode" required/>
    </label>
    <br/>
    <label>
        Phone Number
        <input type="text" name="phoneNumber" required/>
    </label>
    <br/>
    <input type="button" value="register" onclick="pass()"/>
    <input type="submit" hidden="true"/>

</form>

</body>
</html>