<%--
  Created by IntelliJ IDEA.
  User: conoz
  Date: 3/16/2019
  Time: 7:48 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>L.I. Wash & Fold</title>

    <link rel="stylesheet" href="./assets/css/animate-3.7.0.css">
    <link rel="stylesheet" href="./assets/css/font-awesome-4.7.0.min.css">
    <link rel="stylesheet" href="./assets/fonts/flat-icon/flaticon.css">
    <link rel="stylesheet" href="./assets/css/bootstrap-4.1.3.min.css">
    <link rel="stylesheet" href="./assets/css/owl-carousel.min.css">
    <link rel="stylesheet" href="./assets/css/nice-select.css">
    <link rel="stylesheet" href="./assets/css/style.css">

    <!-- Javascript -->
    <script src="./assets/js/vendor/jquery-2.2.4.min.js"></script>
    <script src="./assets/js/vendor/bootstrap-4.1.3.min.js"></script>
    <script src="./assets/js/vendor/wow.min.js"></script>
    <script src="./assets/js/vendor/owl-carousel.min.js"></script>
    <script src="./assets/js/vendor/jquery.nice-select.min.js"></script>
    <script src="./assets/js/vendor/ion.rangeSlider.js"></script>
    <script src="./assets/js/main.js"></script>
</head>
<body>

<!-- Header Area Starts -->
<header class="header-area single-page">
    <div class="header-top">
        <div class="container">
            <div class="row">
                <div class="col-lg-2">
                    <div class="logo-area">
                        <a href="./index.jsp">L.I. Wash & Fold</a>
                    </div>
                </div>
                <div class="col-lg-10">
                    <div class="custom-navbar">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                    <div class="main-menu main-menu-light">
                        <ul>
                            <li class="active"><a href="./index.jsp">home</a></li>
                            <li><a href="./aboutUs.jsp">about us</a></li>
                            <li><a href="./prices.jsp">prices</a></li>
                            <li><a href="./howTo.jsp">how it works</a></li>
                            <li class="menu-btn">
                                <a href="./login.jsp" class="login">log in</a>
                                <a href="./signUp.jsp" class="template-btn">register</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="page-title text-center">
        <div class="container">
            <div class="row">
                <div class="d-flex justify-content-center align-items-center container">
                    <div class="row ">
                        <form action="./login" method="post">
                            <div class="form-group">
                                <label for="email">Email address</label>
                                <input type="email" class="form-control" id="email" name="email" aria-describedby="emailHelp" placeholder="Enter email">
                            </div>
                            <div class="form-group">
                                <label for="password">Password</label>
                                <input type="password" class="form-control" id="password" name="password" placeholder="Password">
                            </div>
                            <div class="form-check">
                                <input type="checkbox" class="form-check-input" id="exampleCheck1">
                                <label class="form-check-label" for="exampleCheck1">Remember Me</label>
                            </div>
                            <button type="submit" class="btn btn-primary">Log In</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>

</body>
</html>