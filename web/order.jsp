<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: conoz
  Date: 3/16/2019
  Time: 6:10 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>L.I. Wash & Fold</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>

Start Your Order:


<form action="./submitOrder" id="orderForm" method="post">
    <label>
        Detergent Type
        <select name="detergent">
            <c:forEach items="${detergentList}" var="detergent">
                <option value="${detergent.id}">${detergent.name}</option>
            </c:forEach>
        </select>
    </label>
    <br/>
    <label>
        Fabric Softener Type
        <select name="fabricSoftener">
            <c:forEach items="${fabricSoftenerList}" var="fabricSoftener">
                <option value="${fabricSoftener.id}">${fabricSoftener.name}</option>
            </c:forEach>
        </select>
    </label>
    <br/>
    <label>
        Pick up method
        <select name="pickUpMethod">
            <c:forEach items="${pickUpMethodList}" var="pickUpMethod">
                <option value="${pickUpMethod.id}">${pickUpMethod.name}</option>
            </c:forEach>
        </select>
    </label>
    <br/>
    <label>
        Drop Off method
        <select name="dropOffMethod">
            <c:forEach items="${dropOffMethodList}" var="dropOffMethod">
                <option value="${dropOffMethod.id}">${dropOffMethod.name}</option>
            </c:forEach>
        </select>
    </label>
    <br/>
    <label>
        Special Instructions
        <input type="text" name="specialInstructions" />
    </label>
    <br/>
    <label>
        Preferred Pick Up Time
        <input type="datetime-local" name="pickupTime" required/>
    </label>
    <br/>
    <label>
        Separate Whites?
        <select name="separateWhites">
            <option value="0">No</option>
            <option value="1">Yes</option>
        </select>
    </label>
    <br/>
    <label>
        Separate Bags?
        <select name="separateBags">
            <option value="0">No</option>
            <option value="1">Yes</option>
        </select>
    </label>
    <br/>
    <input type="submit" value="Place Order" />

</form>

</body>
</html>
